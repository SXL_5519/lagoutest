import os
from time import sleep

import allure
import pytest as pytest

from web_test.base import Base
from read_yaml import Read_Yaml


class Test_Index():
    data = Read_Yaml(os.path.split(os.path.split(__file__)[0])[0] + '/data/data.yaml')
    # @allure.title(cases)
    @pytest.mark.parametrize('cases', data.data['data']['case'])
    def test_register(self, stat_driver, cases):
        base = Base(stat_driver, self.data.data)
        # base.get_url()
        for step in cases['case_step']:
            with allure.step(step['name']):
                base.steps(step)
