import os
import sys
import time

import pytest as pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webdriver import WebDriver

from read_yaml import Read_Yaml

f = sys.path
data = Read_Yaml(os.path.split(__file__)[0] + './data/data.yaml')
_base_url = data.read_yaml('data','base_url')

@pytest.fixture()
def stat_driver(driver: WebDriver = None):
    if driver is None:
        driver = webdriver.Chrome()
        driver.maximize_window()
    # driver.implicitly_wait(5)
    driver.get(_base_url)
    if 'cookies' in data.data['data'].keys():
        cookies = data.data['data']['cookies']
        for cookie in cookies:
            driver.add_cookie(cookie)
        driver.get(_base_url)
    yield driver
    print("关闭浏览器")
    driver.quit()