import logging

import yaml


class Read_Yaml:
    def __init__(self,path):
        file = open(path, 'r', encoding="utf-8")
        file_data = file.read()
        file.close()
        self.data = yaml.safe_load(file_data)

    def read_yaml(self,*value):
        data = self.data
        if value:
            for value in value:
                if not isinstance(data, list):
                    data = data[value]
                else:
                    raise KeyError('value只支持key值')
        else:
            data = self.data
        return data

    def set_listdata(slef,key):
        """
        将对应的值添加到list
        :param key:
        :return:
        """
        names = []
        for name in slef.read_yaml(key):
            names.append(name['name'])
        return names

    def set_yaml(self, path, name, data):
        """

        :param data:
        :return:
        """
        file = open(path, 'w+', encoding="utf-8")
        self.data['data'][name] = data
        file.write(self.data)
        file.close()

# if __name__ == '__main__':
#     Read_Yaml('./web_test/data.yaml', 'data', 'key2',)
