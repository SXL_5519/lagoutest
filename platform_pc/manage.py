from gevent import monkey
from app import create_app
from gevent.pywsgi import WSGIServer

from app.doc.readConfig import ReadConfig

monkey.patch_all()

"""
 development:    开发环境
 production:     生产环境
 testing:        测试环境
 default:        默认环境
 
"""
readconfig = ReadConfig()
app = create_app(readconfig.get_environment('environment', 'environment'))

if __name__ == '__main__':
    # http_server = WSGIServer(('127.0.0.1', 5051), app)
    # http_server.serve_forever()
    app.run('127.0.0.1', 5051)
