import logging
from app.doc.readConfig import ReadConfig


class Config(object):
    readconfig = ReadConfig()
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True
    level = readconfig.get_log('LOG', 'LOG_LEVEL')
    if level == 'INFO':
        LOG_LEVEL = logging.INFO
    elif level == 'DEBUG':
        LOG_LEVEL = logging.DEBUG
    elif level == 'ERROR':
        LOG_LEVEL = logging.ERROR
    elif level == 'CRITICAL':
        LOG_LEVEL = logging.CRITICAL
    elif level == 'WARNING':
        LOG_LEVEL = logging.WARNING
    elif level == 'NOTSET':
        LOG_LEVEL = logging.NOTSET
    ProxyUser = readconfig.get_proxy('Proxy', 'proxyUser')
    ProxyPass = readconfig.get_proxy('Proxy', 'proxyPass')
    Proxy = readconfig.get_proxy('Proxy', 'proxy')
    db_user = readconfig.get_db('db', 'username')
    db_password = readconfig.get_db('db', 'password')
    db_host = readconfig.get_db('db', 'host')
    db_port = readconfig.get_db('db', 'port')
    db_database = readconfig.get_db('db', 'database')
    SQLALCHEMY_POOL_SIZE = int(readconfig.get_db('db', 'pool_size'))
    SQLALCHEMY_POOL_TIMEOUT = int(readconfig.get_db('db', 'pool_time'))
    SQLALCHEMY_MAX_OVERFLOW = int(readconfig.get_db('db', 'max_overflow'))

# 开发环境的配置
class DevelopmentConfig(Config):
    DEBUG = True

    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://zhangliantong:5jrCmjdKOBNm94HxTcGJ@118.178.121.50:43003/enterprise'
    SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{Config.db_user}:{Config.db_password}@{Config.db_host}:{Config.db_port}/{Config.db_database}'


# 测试环境
class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{Config.db_user}:{Config.db_password}@{Config.db_host}:{Config.db_port}/{Config.db_database}'


# 生产环境的配置
class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{Config.db_user}:{Config.db_password}@{Config.db_host}:{Config.db_port}/{Config.db_database}'


# 初始化app实例时对应的开发环境声明
config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig
}
