from flask import jsonify
from flask_restful import Api
from app.api.views import get_query_data
from app.api import api

# 创建应用
api_obj = Api(api)

api_obj.add_resource(get_query_data, '/query/data')

@api.app_errorhandler(404)
def page_not_found(e):
    """这个handler可以catch住所有abort(404)以及找不到对应router的处理请求"""
    return jsonify({'error': '没有找到您想要的资源', 'code': '404', 'data': ''})

@api.app_errorhandler(400)
def page_not_found(e):
    """这个handler可以catch住所有abort(404)以及找不到对应router的处理请求"""
    return jsonify({'error': '参数错误', 'code': '400', 'data': ''})


@api.app_errorhandler(500)
def internal_server_error(e):
    """这个handler可以catch住所有的abort(500)和raise exeception."""
    return jsonify({'error': '服务器内部错误', 'code': '500', 'data': ''})


