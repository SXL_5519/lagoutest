import json
import time

from flask import make_response, jsonify, request
from flask_restful import Resource
from werkzeug.exceptions import abort
from app.api import api
from app.doc.data_treat import data_treat
from app.models import data_treating, select_data_one, select_data_like
from app.doc.get_data import get_all_data
import logging


@api.route('/query/', methods=['POST'])
def query_data():
    """
     企业数据查询
    """
    try:
        form_data = json.loads(request.get_data(as_text=True))
    except:
        form_data = dict(request.form)
    logging.info(f'接收的参数是{form_data}')
    if 'keyword' in form_data.keys():
        select_data = select_data_one(form_data['keyword'])
        if select_data:
            all_data = select_data
        else:
            data = get_all_data().get_list_data(form_data['keyword'])
            logging.info(f"获取的数据为：{data}")
            all_data = data_treat(data)
            logging.info(f"接口返回data数据：{all_data}")
        return jsonify(code=200,
                       msg="success",
                       data=all_data)
    else:
        abort(400)


class get_query_data(Resource):

    def get(self):
        return 'get'

    def post(self):
        """
             企业数据查询
        """
        try:
            form_data = json.loads(request.get_data(as_text=True))
        except:
            form_data = dict(request.form)
        logging.info(f'接收333的参数是{form_data}')
        if 'keyword' in form_data.keys():
            select_data = select_data_one(form_data['keyword'])
            if select_data:
                all_data = select_data
            else:
                data = get_all_data().get_list_data(form_data['keyword'])
                logging.info(f"获取的数据为：{data}")
                all_data = data_treat(data)
                logging.info(f"接口返回data数据：{all_data}")
            return jsonify(code=200,
                           msg="success",
                           data=all_data)
        else:
            abort(400, '参数错误')
