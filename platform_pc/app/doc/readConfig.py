# -*- coding:utf-8 -*-
import configparser
import os

"""
读取配置文件的数据
"""

path = os.path.abspath(os.path.dirname(__file__)).split('platform_pc')[0]
configPath = path + "platform_pc/config.ini"


class ReadConfig:
    def __init__(self):
        self.cf = configparser.ConfigParser()
        self.cf.read(configPath, encoding='utf-8')

    def get_environment(self, module, name):
        value = self.cf.get(module, name)
        return value

    def get_proxy(self, module, name):
        value = self.cf.get(module, name)
        return value

    def get_db(self, module, name):
        value = self.cf.get(module, name)
        return value

    def get_log(self, module, name):
        value = self.cf.get(module, name)
        return value
