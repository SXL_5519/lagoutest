from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from config import config
import pymysql
# 创建数据库
from logs.logs import setup_log

db = SQLAlchemy()


def create_app(config_name):
    from app.resource import api_obj
    # 初始化
    setup_log(config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    pymysql.install_as_MySQLdb()
    # 初始化扩展（数据库）
    db.init_app(app)
    api_obj.init_app(app)
    regist_blueprints(app)
    # # 创建数据库表
    create_tables(app)
    # 注册所有蓝本
    return app


def regist_blueprints(app):
    # from app.api import api
    # app.register_blueprint(api, url_prefix='/api')
    from app.resource import api
    app.register_blueprint(api, url_prefix='/api')


def create_tables(app):
    """
    根据模型，创建表格
    """
    from app.models import BusinessInformation
    db.create_all(app=app)
