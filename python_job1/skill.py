"""
技能
"""
import random


class Skill:

    def fight(self,hero_one,hero_too):
        """
        一回合两个英雄血量对比
        :param hero_one: 英雄1的属性值
        :param hero_too: 英雄2的属性值
        :return:
        """

        hero_one_hp = hero_one['hp'] - hero_too['power'][random.randint(0,len(hero_too['power'])-1)]
        hero_too_hp = hero_too['hp'] - hero_one['power'][random.randint(0,len(hero_one['power'])-1)]
        if hero_one_hp > hero_too_hp:
            print(f'{hero_one["name"]}赢了')
        elif hero_one_hp < hero_too_hp:
            print(f'{hero_too["name"]}赢了')
        else:
            print('平局')

    def speak_lines(self,name,lins):
        """
        开场白
        :param lins:
        :return:
        """
        print(f'{name}:{lins}')