import random

import yaml

from python_job1.hero import Hero


class Hero_factory:
    def __init__(self):
        file = open('./hero.yaml', 'r', encoding="utf-8")
        file_data = file.read()
        file.close()
        self.data = yaml.load(file_data)

    def create_hero(self):
        hero = Hero()
        hero_lists = []
        if len(self.data['hero']) > 1:
            hero_nu = random.sample(range(len(self.data['hero'])), 2)
            for i in hero_nu:
                hero_lists.append(hero.return_attribute(self.data['hero'][i]))
                hero.speak_lines(self.data['hero'][i]['name'],self.data['hero'][i]['prologue'])
            hero.fight(hero_lists[0],hero_lists[1])
        else:
            print(f'未匹配到足够的英雄')

if __name__ == '__main__':
    hero = Hero_factory()
    hero.create_hero()