import os
import sys
import time

import pytest as pytest
from appium import webdriver
from appium.webdriver.webdriver import WebDriver

from read_yaml import Read_Yaml

f = sys.path
data = Read_Yaml(os.path.split(__file__)[0] + './data/data.yaml')
_base_url = data.read_yaml('data','base_url')

@pytest.fixture()
def stat_driver(driver: WebDriver = None):
    if driver is None:
        print("driver == None, 创建driver")
        caps = {}
        caps["platformName"] = "Android"
        caps["deviceName"] = "hogwarts"
        # Mac/Linux: adb logcat |grep -i activitymanager (-i忽略大小写)
        # Windows:  adb logcat |findstr /i activitymanager
        caps["appPackage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.LaunchSplashActivity"
        # 防止清空缓存-比如登录信息
        caps["noReset"] = "true"
        # 最重要的一步，与server 建立连接
        driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
        # 隐式等待 5 秒
        driver.implicitly_wait(60)
    # driver.implicitly_wait(5)
    # if 'cookies' in data.data['data'].keys():
    #     cookies = data.data['data']['cookies']
    #     for cookie in cookies:
    #         driver.add_cookie(cookie)
    #     driver.get(_base_url)
    yield driver
    print("关闭浏览器")
    driver.quit()