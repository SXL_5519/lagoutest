import pytest as pytest
import yaml
from python_job2.counter import Counter

@pytest.fixture(scope='class')
def calculator():
    print('类计算器开始')
    counter = Counter()
    yield counter
    print('类计算器结束')


def pytest_collection_modifyitems(items):
    """
    测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上
    :return:
    """
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")