class Counter:
    def add(self,a, b):
        return round(a + b,2)


    def sub(self,a, b):
        return round(a - b,2)


    def multiplication(self,a, b):
        return round(a * b)


    def division(self,a, b):
        if b <= 0 :
            raise ValueError('被除数小于等于0')
        else:
            return round(a / b)

