import allure
import pytest as pytest
from read_yaml import Read_Yaml

@allure.feature('计算器')
class Testcointer:
    data = Read_Yaml('./data.yaml')

    @allure.story('加法')
    @pytest.mark.parametrize('a',data.read_yaml('add'), ids=data.set_listdata('add'))
    def test_add(self,calculator,a):
        with allure.step('输入2个数字并断言'):
            assert float(a['c']) ==calculator.add(a['a'], a['b'])

    @allure.step('做减法')# 测试步骤，装饰器只能作用到类或方法上，with 可放到，代码块
    @allure.story('减法')
    @pytest.mark.parametrize('a',data.read_yaml('sub'), ids=data.set_listdata('sub'))
    def test_sub(self,calculator,a):
        assert float(a['c']) ==calculator.sub(a['a'], a['b'])

    @allure.story('乘法')
    @pytest.mark.parametrize('a',data.read_yaml('multiplication'), ids=data.set_listdata('multiplication'))
    def test_multiplication(self,calculator,a):
        assert float(a['c']) ==calculator.multiplication(a['a'], a['b'])

    @allure.story('除法')
    @pytest.mark.parametrize('a',data.read_yaml('division'), ids=data.set_listdata('division'))
    def test_division(self,calculator,a):
        assert float(a['c']) ==calculator.division(a['a'], a['b'])
